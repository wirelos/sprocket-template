# Sprocket Mesh

## Interact with the mesh
As an examplte of how to interact with the mesh, we use Mosquitto pub and sub tools.  
Publish on a channel:  
```sh
mosquitto_pub -h broker.lan -t wirelos-in  -m "lorem ipsum"

docker exec $MOSQUITTO_CONTAINER mosquitto_pub -h localhost -t wirelos-in/broadcast/  -m "broadcastenen"
```
Subscribe to a channel:
```sh
mosquitto_sub -v -h localhost -t wirelos-in/#
```

## OTA deployment with Docker
```sh
# sample usage to deploy a sprocket
source sprockets/illucat && ./ci/deploy-fw.sh

# a sprocke config contains target device settings
export FIRMWARE_ID=2
export TOPIC=wirelos-
export BROKER=citadel.lan
./ci/build-fw.sh
./ci/deploy-fw.sh
```
## Firmware configuration
A sample configuration is provided in `src/firmware/sprocket.h.example`.
  
## Mesh and MQTT configuration

The following variables should be set:
 - *NETWORK_PASSWORD* : Specifies the password of your wireless network
 - *NETWORK_LIST* : Specifies the SSIDs of your wireless network
 - *MQTT_SERVER* : Specify the IP address of your MQTT broker
 - *MQTT_PORT* : Specify the port of your MQTT broker

The following can optionally be changed:
 - *MESH_PASSWORD* : A string used by all nodes to prevent unauthorized access to the mesh network
 - *MESH_PORT* : The port that the mesh nodes listen on

These options are only relevant if the node is compiled with SSL enabled:
 - *MESH_SECURE* : Enable SSL bewteen mesh nodes.  This requires that the `ssl_cert.h` file is present (Use [this](https://github.com/marvinroger/async-mqtt-client/blob/master/scripts/gen_server_cert.sh) script to generate this header)
 - *MQTT_SECURE* : Enable SSL connection to the MQTT broker.
 - *MQTT_FINGERPRINT* : a 20-byte string that uniquely identifies your MQTTbroker.  A script to retrieve the fingerprint can be found [here](https://github.com/marvinroger/async-mqtt-client/blob/master/scripts/get-fingerprint/get-fingerprint.py)
   
## Compiling and uploading
This example has been designed to use platformio for building and install
Assuming you have already setup a platformio environment:

### Non-SSL
`platformio run --target upload`

### SSL (Still experimental)
`platformio run -e ssl --target upload`
