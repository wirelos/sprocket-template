#ifndef _APP_H_
#define _APP_H_

template <class T>
class App {
    private:
        T* stack;
    public:
        App();
        App(T* stack);
};

#endif