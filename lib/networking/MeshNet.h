#ifndef _MESH_NET_H_
#define _MESH_NET_H_


// FIXME: cleanup that global mess

#include "sprocket.h"
#include "firmware/id.h"
#include <FS.h>
#include <ESP8266MQTTMesh.h>

wifi_conn    networks[]       = NETWORK_LIST;
const char*  mesh_password    = MESH_PASSWORD;
const char*  mqtt_server      = MQTT_SERVER;
const int    mqtt_port        = MQTT_PORT;
#if ASYNC_TCP_SSL_ENABLED
const uint8_t *mqtt_fingerprint = MQTT_FINGERPRINT;
bool mqtt_secure = MQTT_SECURE;
  #if MESH_SECURE
  #include "ssl_cert.h"
  #endif
#endif

// Note: All of the '.set' options below are optional.  The default values can be
// found in ESP8266MQTTMeshBuilder.h
ESP8266MQTTMesh buildMesh() { 
  return ESP8266MQTTMesh::Builder(networks, mqtt_server, mqtt_port)
                       .setVersion(FIRMWARE_VER, FIRMWARE_ID)
                       .setMeshPassword(mesh_password)
                       .setTopic(TOPIC_IN, TOPIC_OUT)
#if ASYNC_TCP_SSL_ENABLED
                       .setMqttSSL(mqtt_secure, mqtt_fingerprint)
#if MESH_SECURE
                       .setMeshSSL(ssl_cert, ssl_cert_len, ssl_key, ssl_key_len, ssl_fingerprint)
#endif //MESH_SECURE
#endif //ASYNC_TCP_SSL_ENABLED
                       .build();

}

ESP8266MQTTMesh MESH = buildMesh();

class MeshNet {
    public:
        ESP8266MQTTMesh* net;
         // FIXME buildMesh is still ugly
        MeshNet(){
            net = &MESH;
            net->begin();
        }
        void onMsg(void(*meshCallback)(const char *topic, const char *msg)){
            net->setCallback(meshCallback);
        }

};

#endif