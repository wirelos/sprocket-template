#ifndef _WIFI_NET_H_
#define _WIFI_NET_H_

#if defined(ESP8266)
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <ESP8266mDNS.h>
#else
#include <WiFi.h>
#endif

#include <WebStack.h>
#include <DNSServer.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>         //https://github.com/tzapu/WiFiManager

#include "sprocket.h"

class WiFiNet {
    private:
        AsyncWiFiManager* wifiManager;
        AsyncWebServer* server;
        DNSServer* dns;
    public:
        WiFiNet(){
            //server = new AsyncWebServer(80);
            dns = new DNSServer();
        }
        WiFiNet* use(WebStack* stack) {
            server = stack->server;
            return this;
        }
       void connect(){
            wifiManager = new AsyncWiFiManager(server, dns);
            wifiManager->autoConnect(AP_NAME);
            Serial.println("Hostname: " + String(HOST_NAME));
            WiFi.hostname(HOST_NAME);
            startMDNS();
        } 
        void startMDNS(){
            if (!MDNS.begin(HOST_NAME)) {
                Serial.println("Error setting up MDNS responder!");
            } else {
                Serial.println("mDNS responder started");
                MDNS.addService("http", "tcp", 80);
            }
        }

};

#endif