#ifndef __APPSTACK_H_INCLUDED__
#define __APPSTACK_H_INCLUDED__ 

#include <Arduino.h>
#include <Scheduler.h>
#include <ESPAsyncWebServer.h>

class AppStack {
    public:
        SchedulerClass* scheduler;
        AppStack(){
            scheduler = &Scheduler;
        }
        void begin() {
            scheduler->begin();
        }
};

#endif