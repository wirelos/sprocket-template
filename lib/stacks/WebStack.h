#ifndef __WEBSTACK_H_INCLUDED__
#define __WEBSTACK_H_INCLUDED__ 

#include <Arduino.h>
#include <ESPAsyncWebServer.h>
#include <AppStack.h>
#include <StackUtils.h>

#include "sprocket.h"

class WebStack : public AppStack {
    public:
        AsyncWebServer* server;
        AsyncWebSocket* socket;
        WebStack() : AppStack(){
            server = new AsyncWebServer(80);
            initWebServer();
        }
        WebStack(AsyncWebServer* webServer) : AppStack(){
            server = webServer;
            initWebServer();
        }
        WebStack(AsyncWebServer* webServer, AsyncWebSocket* webSocket) : AppStack(){
            server = webServer;
            socket = webSocket;
            initWebServer();
        }
        void initWebServer(){
            server->serveStatic(WEBSERVER_ROOT, SPIFFS, WEBSERVER_HTDOCS).setDefaultFile(WEBSERVER_DEFAULT_FILE);
            server->on(WEBCONFIG_GET_HEAP, HTTP_GET, HTTP_GET_HEAP);
        }
        void begin() {
            server->begin();
            AppStack::begin();
        }
};

#endif