#ifndef _DELTA_TIMER_H_
#define _DELTA_TIMER_H_

class DeltaTimer {
private:
  unsigned long ms = 0;
  unsigned long time;
public:
  void setTime(unsigned long updateTime) {
    time = updateTime;
  }

  bool update(unsigned long delta) {
    ms += delta;
    if (ms > time) {
      ms = ms % time;
      return true;
    }
    return false;
  }
};

#endif