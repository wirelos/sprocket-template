#ifndef _STACK_UTILS_
#define _STACK_UTILS_

#include <ESPAsyncWebServer.h>
#include <ESP8266MQTTMesh.h>

static void HTTP_GET_HEAP(AsyncWebServerRequest *request) {
    request->send(200, "text/plain", String(ESP.getFreeHeap()));
}

static void MQTT_PUBLISH_INFO(ESP8266MQTTMesh* mesh, String* nodeId) {
    String msg = WiFi.localIP().toString() + " / " + String(ESP.getFreeHeap());
    mesh->publish_node(nodeId->c_str(), msg.c_str());
}

#endif

