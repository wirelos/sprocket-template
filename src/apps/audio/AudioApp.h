#ifndef _AUDIOSOCKETAPP_H_INCLUDED__
#define _AUDIOSOCKETAPP_H_INCLUDED__

#include <Scheduler.h>
#include <AppStack.h>
#include "AudioTask.h"

class AudioApp {
    public:
    AudioTask* task;
    AudioApp(){
        task = new AudioTask();
    }
    AudioTask* use(AppStack* stack) {
        stack->scheduler->start(task);
        return task;
    }
        
};
#endif