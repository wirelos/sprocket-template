#include "AudioTask.h"

AudioTask::AudioTask() {

}

/* void AudioTask::onSample(void(*fp)(int sample)){
    callback = fp;
} */

void AudioTask::loop() {
    startMillis= millis();
    peakToPeak = 0;
    signalMax = 0;
    signalMin = 1024;
    // collect data 
    while (millis() - startMillis < sampleWindow) {
        sample = analogRead(AUDIO_PIN);
        if (sample < 1024) {
            signalMax = sample > signalMax ? sample : signalMax; 
            signalMin = sample < signalMin ? sample : signalMin;
            
        }
    }
    peakToPeak = signalMax - signalMin;
    if(peakToPeak != prevPeakToPeak && peakToPeak > 0 
        && (prevPeakToPeak + 2 < peakToPeak || prevPeakToPeak - 2  > peakToPeak)){
        callback(peakToPeak);
    }
    prevPeakToPeak = peakToPeak;
    delay(128);
    yield();
}
