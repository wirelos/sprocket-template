#ifndef __AUDIOTASK_H_INCLUDED__
#define __AUDIOTASK_H_INCLUDED__

#include <Scheduler.h>
#include <ESPAsyncWebServer.h>
#include <AsyncWebSocket.h>
#include <ESPAsyncTCP.h>
#include <WsUtils.h>

#include "sprocket.h"

using namespace std;

/**
 * Task to read the pepak-peak amplitude from a analog input AUDIO_PIN.
 * 
 * FIXME: poorly performing and readings are blocking...
 */
class AudioTask: public Task {
    private:
        struct TState {
            String data;
        } tState;
        const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
        unsigned int sample;
        unsigned long startMillis= millis(); 
        unsigned int peakToPeak = 0; 
        unsigned int signalMax = 0;
        unsigned int signalMin = 1024;
        unsigned int prevPeakToPeak = 0;
        void(*callback)(int reading);
        AsyncWebSocket *ws;
    public:
        AudioTask();
        //void onSample(void(*fp)(int reading));
        std::function<void(int reading)> onSample;
    protected:
        void loop();
};

#endif