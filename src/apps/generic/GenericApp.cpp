#include "GenericApp.h"

GenericApp::GenericApp() {
    task = new GenericTask();
}

GenericApp* GenericApp::use(AppStack* stack) {
    stack->scheduler->start(task);
    return this;
}

GenericApp* GenericApp::use(WebStack* stack) {
    stack->scheduler->start(task);
    return this;
}
