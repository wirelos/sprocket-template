#ifndef _GENERIC_APP_H
#define _GENERIC_APP_H

#include "Scheduler.h"
#include "WebStack.h"
#include "GenericTask.h"

class GenericApp {
    private:
        GenericTask* task;
    public:
        GenericApp();
        GenericApp* use(AppStack* stack);
        GenericApp* use(WebStack* stack);
};

#endif