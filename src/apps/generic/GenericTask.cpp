#include "GenericTask.h"

GenericTask::GenericTask() {

}

void GenericTask::setup() {
    Serial.println("setup task");
}

void GenericTask::loop() {
    Serial.println("loop task");
    yield();
    delay(1000);
}
