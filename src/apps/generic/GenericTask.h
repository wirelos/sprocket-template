#ifndef _GENERIC_TASK_H_
#define _GENERIC_TASK_H_

#include "Scheduler.h"

class GenericTask : public Task {
    public:
        GenericTask();
    protected:
        void setup();
        void loop();
};

#endif