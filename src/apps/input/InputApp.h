#ifndef _InputApp_H_
#define _InputApp_H_

#include <AppStack.h>
#include <MeshNet.h>

#include "InputTask.h"
#include "sprocket.h"

using namespace std;

typedef struct InputMapping {
    String pin;
    int inputType;
};

class InputApp {
    public:
        InputTask* inputTask;
        MeshNet* mesh;
        std::function<void(const char *topic, const char *msg)> callback;

        InputApp(){
            inputTask = new InputTask();
        }
        InputApp* use(AppStack* stack){
            stack->scheduler->start(inputTask);
            return this;
        }
        // FIXME clean that up for sake of consistency
        InputApp* use(MeshNet* meshNet){
            mesh = meshNet;
            callback = [=](const char *topic, const char *msg){
                Serial.println(msg);
            };
            mesh->net->setCallback(callback);
            inputTask->onChange = [=](String pin, int value){
                mesh->net->publish(String(pin).c_str(), String(value).c_str());
            };
            return this;
        }
};

#endif  