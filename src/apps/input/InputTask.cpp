#include "InputTask.h"

InputTask::InputTask(){
    onChange = [=](String pin, int value) {
        Serial.println("default callback");
    };
}

void InputTask::setup(){
    pinMode(D3, INPUT_PULLUP);
}

void InputTask::loop(){
    if(digitalRead(D3)){
        state = !state;
        onChange("D3", state);
    }
    yield();
    delay(150);
}