#ifndef _INPUTTASK_H_
#define _INPUTTASK_H_

#include <Scheduler.h>

using namespace std;

class InputTask : public Task {
    public:
        InputTask();
        std::function<void(String pin, int value)> onChange;
        int state = 0;
    protected:
        void setup();
        void loop();
};

#endif