#ifndef _NEOPATTERNAPP_H_
#define _NEOPATTERNAPP_H_


#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncWebSocket.h>
#include <WsUtils.h>
#include <App.h>

#include "NeoPatternTask.h"
#include "NeoPattern.h"
#include "NeoPatternWeb.h"
#include <WebStack.h>
#include <MeshNet.h>

#include "sprocket.h"

using namespace std;

/* template <class WebStack>
 */
class NeoPatternApp /* : public App<WebStack> */ {
    private:
    public:
        NeoPattern* pixels;
        NeoPatternTask* task;
        NeoPatternWeb* web;
        std::function<void(const char *topic, const char *msg)> callback;

        NeoPatternApp(){
            pixels = new NeoPattern(LED_STRIP_LENGTH, LED_STRIP_PIN, NEO_GRB + NEO_KHZ800, [](int pixels){});
            task = new NeoPatternTask(pixels);
        }

        NeoPatternApp* use(AppStack* stack) {
            stack->scheduler->start(task);
            return this;
        }

        NeoPatternApp* use(MeshNet* mesh) {
            callback = [=](const char *topic, const char *msg){
                setHexColor(msg);
            };
            mesh->net->setCallback(callback);
            return this;
        }

        void use(WebStack* stack) {
            web = new NeoPatternWeb(stack->server, task);
            stack->scheduler->start(task);
        }
        
        void onAnimationComplete(void(*fp)(int pixels)){
            pixels->OnComplete = fp;
        }

        /**
         * message in format char *msg = "0000FF";
         */
        void setHexColor(const char *hex){
            int r, g, b;
            /* if(strcmp(hex[0], "#") == 0)){
                sscanf(hex, "#%02x%02x%02x", &r, &g, &b);
            } */ 
            sscanf(hex, "%02x%02x%02x", &r, &g, &b);
            task->stop();
            pixels->ColorSet(pixels->Color(r,g,b));
        }
};

#endif