#include "NeoPatternTask.h"

NeoPatternTask::NeoPatternTask(NeoPattern* np) {
    strip = np;
}

void NeoPatternTask::setup() {
    strip->begin();
    strip->setBrightness(tState.brightness);
    strip->completed = 1;
    //initTask();
}

void NeoPatternTask::loop()  {
    update();
    completed();
}

void NeoPatternTask::stop(){
    strip->ActivePattern = NONE;
    tState.mode = IDLE;
}

void NeoPatternTask::progressColorState(int doFade) {
    tState.color1 = tState.color2;
    tState.color2 = tState.data.toInt();
    if(doFade){
        tState.mode = fade(tState.color1, tState.color2);
    } else {
        strip->ColorSet(tState.color2);
        stop();
    }
}

void NeoPatternTask::update() {
    switch(tState.mode) {
        case COLOR: 
            progressColorState(0);
            break;
        case COLOR2: 
            progressColorState(1);
            break;            
        case BRIGHTNESS:
            tState.brightness = tState.data.toInt();
            tState.mode = brightness(tState.brightness);
            break;
        case PATTERN:
            tState.mode = pattern(tState.pattern);
            break;
    }
    strip->Update();
}

void NeoPatternTask::completed(){
    if(tState.mode != PATTERN && strip->completed){
        strip->ActivePattern = NONE;
        strip->completed = 0;
    }
}

NeoPatternTask::TaskMode NeoPatternTask::fade(int fromColor, int toColor) {
    strip->Fade(fromColor, toColor, 300, 10);
    return IDLE;
}
NeoPatternTask::TaskMode NeoPatternTask::brightness(int brightness) {
    // FIXME set brightness by reducing color intensity
    strip->setBrightness(brightness);
    return IDLE;
}
NeoPatternTask::TaskMode NeoPatternTask::pattern(char pattern) {
    if(strip->completed) {
        switch(pattern) {
            case RAINBOW:
                strip->RainbowCycle(50);
                break;
            case SCANNER:
                strip->setBrightness(tState.brightness);
                strip->Scanner(tState.color1, 100);
                break;
            case THEATER:
                strip->setBrightness(tState.brightness);
                strip->TheaterChase(tState.color1, tState.color2, 500);
                break;                
        }
    }
    return PATTERN;
}

void NeoPatternTask::setPattern(TaskMode m, Pattern p) {
    strip->completed = 1;
    tState.mode = m;
    tState.pattern = p;
}

void NeoPatternTask::initTask() {
    /* // load config from json file
    // TODO create JsonBundle, see https://github.com/bblanchon/ArduinoJson/issues/308
    File configFile = esp->spiffs->readFile("/config/init.json");
    size_t size = configFile.size();
    std::unique_ptr<char[]> buf(new char[size]);
    configFile.readBytes(buf.get(), size);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.parseObject(buf.get());
    // set initial state      
    JsonVariant tMode = json["mode"];
    if (tMode.success()) {
        tState.mode = (TaskMode)tMode.as<char*>()[0];
    }
    JsonVariant tPattern = json["pattern"];
    if (tPattern.success()) {
        tState.pattern = (Pattern)tPattern.as<char*>()[0];
    } */
}
void NeoPatternTask::saveState() {
    /* // create json
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["mode"] = (char)tState.mode;
    json["pattern"] = (char)tState.pattern;
    // save config to json file
    esp->spiffs->saveJsonConfig("/config/init.json", json); */
}