#ifndef _NEOPATTERNTASK_H_INCLUDED__
#define _NEOPATTERNTASK_H_INCLUDED__

#include <Scheduler.h>
#include <Adafruit_NeoPixel.h>

#include "NeoPattern.h"
#include "colors.h"
#include "sprocket.h"

class NeoPatternTask : public Task {
    public:
        enum TaskMode {
            IDLE = 'I', 
            COLOR = 'C' ,
            COLOR2 = 'D' ,
            BRIGHTNESS = 'B',
            PATTERN = 'P',
            PIXEL = 'X'
        };
        enum Pattern {
            RAINBOW = 'R', 
            SCANNER = 'S' ,
            COLORWIPE = 'W',
            THEATER = 'T'
        };
        struct TState {
            TaskMode mode = PATTERN;
            Pattern pattern = RAINBOW;
            char state = 0;
            int color1 = LED_BLUE_MEDIUM;
            int color2 = LED_BLUE_MEDIUM;
            int brightness = LED_STRIP_BRIGHTNESS;
            String data;
        } tState;
        NeoPattern *strip;
        void completed();
        NeoPatternTask(NeoPattern* np);
        void initTask();
        void saveState();
        /**
         * Fades between two colors.
         * @return next mode = IDLE
         */
        TaskMode fade(int fromColor, int toColor);
        /**
         * Sets the brightness of the whole strip.
         * @return next mode = IDLE
         */
        TaskMode brightness(int brightness);
        TaskMode pattern(char pattern);
        void progressColorState(int doFade);
        void setPattern(TaskMode m, Pattern p);
        void setup();
        void loop();
        void stop();
    protected:
        void update();
};

#endif