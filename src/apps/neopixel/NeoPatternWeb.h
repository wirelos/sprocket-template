#ifndef _NEO_PATTERN_APP_
#define  _NEO_PATTERN_APP_

#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncWebSocket.h>
#include <WsUtils.h>

#include "NeoPatternTask.h"
#include "sprocket.h"

using namespace std;
using namespace std::placeholders;

class NeoPatternWeb {
    private:
        AsyncWebServer* server;
        NeoPatternTask* task;
        AsyncWebSocket* socket;
    public:
        NeoPatternWeb(AsyncWebServer* webServer, NeoPatternTask* task){
            server = webServer;
            task = task;
            socket = new AsyncWebSocket(LED_STRIP_WS_PATTERN);
            bindHandlers();
        }
        void bindHandlers(){
            server->addHandler(socket);
            server->on(LED_STRIP_POST_PATTERN, HTTP_POST, bind(&NeoPatternWeb::setPatternFromRequest, this, _1));
            server->on(LED_STRIP_POST_COLOR, HTTP_POST, bind(&NeoPatternWeb::setColorFromRequest, this, _1));
            socket->onEvent(bind(&NeoPatternWeb::onWebSocketEvent, this, _1, _2, _3, _4, _5, _6));
        }
        void onWebSocketEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
            if(type == WS_EVT_DATA){
                // FIXME change that stupid string parsing and use integers directly
                task->tState.mode = ((NeoPatternTask::TaskMode)(char)data[0]);
                task->tState.data = WsUtils::parseFrameAsString(type, arg, data, len, 1);
                //client->text(String(millis()));
            }
        }
        void setPatternFromRequest(AsyncWebServerRequest *request) {
            NeoPatternTask::TaskMode m = request->hasParam("mode", true) 
                ? (NeoPatternTask::TaskMode)(request->getParam("mode", true)->value()).charAt(0) : NeoPatternTask::TaskMode::PATTERN;
            NeoPatternTask::Pattern p = request->hasParam("pattern", true) 
                ? (NeoPatternTask::Pattern)(request->getParam("pattern", true)->value()).charAt(0) : NeoPatternTask::Pattern::RAINBOW;
            task->setPattern(m,p);
            request->send(200);
        }
        void setColorFromRequest(AsyncWebServerRequest *request) {
            if(request->hasParam("rgb", true)) {
                task->tState.mode = NeoPatternTask::COLOR;
                task->tState.data = request->getParam("rgb", true)->value();
            }
            request->send(200);
        }
};

#endif