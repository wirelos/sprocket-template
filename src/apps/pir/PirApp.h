#ifndef _PIR_APP_H_INCLUDED__
#define _PIR_APP_H_INCLUDED__

#include <Scheduler.h>
#include <ESPAsyncWebServer.h>
#include <AsyncWebSocket.h>
#include <ESPAsyncTCP.h>
#include <MeshNet.h>

#include "PirTask.h"
#include "PirWeb.h"

#include "sprocket.h"

//AsyncWebSocket pirWs(PIR_WS);
//PirTask pirTask(PIR_PIN, &pirWs);

using namespace std;
using namespace std::placeholders;

typedef void(*motionCb)(int);

class PirApp {
    public:
        PirTask* task;
        PirWeb* web;
        MeshNet* mesh;
        
        PirApp(){
            task = new PirTask(PIR_PIN);
        }
        void use(AppStack* stack){
            stack->scheduler->start(task);
        }
        /* PirTask* use(WebStack* stack){
            web = new PirWeb(stack->server, task);
            stack->scheduler->start(task);
            return task;
        }      */
        PirApp* use(MeshNet* meshNet) {
            mesh = meshNet;
            Serial.println("use mesh");
            task->onMotion = [=](int motion){
                mesh->net->publish(String("pir").c_str(), String(motion).c_str());
            };
            return this;
        }          
};

#endif