#ifndef _PIRTASK_H_INCLUDED__
#define _PIRTASK_H_INCLUDED__
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncWebSocket.h>
#include <ESPAsyncTCP.h>
#include <Scheduler.h>

#include "sprocket.h"

using namespace std;

/* volatile int MOTION_DETECTED = 0;
void ISR_MOTION() {
    MOTION_DETECTED = 1;
    Serial.println("motion detected");
} */
/* void DEBUG_MSG(void(thingy)(void)){
    #ifdef DEBUG_MODE
        thingy();
    #endif
} */

class PirTask : public Task {
    private:
        //the time when the sensor outputs a low impulse
        long unsigned int lowIn;         
        boolean lockLow = true;
        boolean takeLowTime;
        AsyncWebSocket* ws;
        int pinPir;
    public:
        std::function<void(int motion)> onMotion;
        //the amount of milliseconds the sensor has to be low 
        //before we assume all motion has stopped
        const long unsigned int pause = 5000;  

        PirTask(int pin, AsyncWebSocket* socket) {
            ws = socket;
            pinPir = pin;
        }
/*         PirTask(int pin, void(*motionCb)(int)) {
            pinPir = pin;
            onMotionCb = motionCb;
        } */
        PirTask(int pin) {
            pinPir = pin;
        }
        /* void onMotion(void(*cb)(int)) {
            onMotionCb = cb;
        } */
    protected:
        void setup() {
            /* EspBaseStack *esp = EspBaseStack::Instance();
            esp->isr->add(pinPir, INPUT, CHANGE, ISR_MOTION); */
            pinMode(pinPir, INPUT);
            digitalWrite(pinPir, LOW);
        }
        void loop() {
            if(digitalRead(pinPir) == HIGH){
                if(lockLow){  
                    //makes sure we wait for a transition to LOW before any further output is made:
                    lockLow = false; 
                    /* DEBUG_MSG([=](){
                        Serial.println("---");
                        Serial.print("motion detected at ");
                        Serial.print(millis()/1000);
                        Serial.println(" sec"); 
                    }); */
                    onMotion(1);
                    
                    this->delay(50);
                }         
                takeLowTime = true;
            }

            if(digitalRead(pinPir) == LOW){       

                if(takeLowTime){
                    lowIn = millis();          //save the time of the transition from high to LOW
                    takeLowTime = false;       //make sure this is only done at the start of a LOW phase
                }
                //if the sensor is low for more than the given pause, 
                //we assume that no more motion is going to happen
                if(!lockLow && millis() - lowIn > pause){  
                    //makes sure this block of code is only executed again after 
                    //a new motion sequence has been detected
                    lockLow = true;                        
                    /* DEBUG_MSG([=](){
                        Serial.print("motion ended at ");      //output
                        Serial.print(millis()/1000);
                        Serial.println(" sec");
                    }); */
                    onMotion(0);
                    
                    this->delay(50);
                }
        }
    }
};

#endif