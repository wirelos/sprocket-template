#ifndef _PIR_WEB_H_INCLUDED__
#define _PIR_WEB_H_INCLUDED__

#include <Scheduler.h>
#include <ESPAsyncWebServer.h>
#include <AsyncWebSocket.h>
#include <ESPAsyncTCP.h>

#include "PirTask.h"

#include "sprocket.h"

//AsyncWebSocket pirWs(PIR_WS);
//PirTask pirTask(PIR_PIN, &pirWs);

using namespace std;
using namespace std::placeholders;

class PirWeb {
    public:
        PirTask* task;
        AsyncWebSocket* socket;
        AsyncWebServer* server;
        PirWeb(AsyncWebServer* webServer, PirTask* tsk){
            task = tsk;
            server = webServer;
            socket = new AsyncWebSocket(PIR_WS);
            server->addHandler(socket);
        }
        void notify(int motion){
            String msg = "{ \"motion:\" : "+String(motion)+ ", \"time\" : "+ String(millis()/1000)+", \"heap\" : \"" + String(ESP.getFreeHeap()) + "\" }";
            Serial.println(msg);
            socket->textAll(msg);
        }
};

#endif