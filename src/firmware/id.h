#ifndef _FIRMWARE_H_
#define _FIRMWARE_H_

#include <Arduino.h>
#include "sprocket.h"

const char*  FIRMWARE_VER = String(SPROCKET_VERSION).c_str();

// ID is used for wifi mesh
#ifdef ESP32
    String ID  = String((unsigned long)ESP.getEfuseMac());
#else
    String ID  = String(ESP.getChipId());
#endif

#endif
