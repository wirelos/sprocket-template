#include <Arduino.h>
#include <FS.h>

#include "sprocket.h"
#include "user_standalone.cpp"
#include "user_mesh.cpp"

void chip_setup() {
    Serial.begin(SERIAL_BAUD);
    SPIFFS.begin();
    delay(STARTUP_DELAY);
}

void setup() {
    chip_setup();
    user_setup();
}

void loop() {
    user_loop();
    yield();
}