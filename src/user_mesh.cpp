#ifdef RUN_MODE_MESH

#include <MeshNet.h>

#include "sprocket.h"
#include "firmware/id.h"

#include STACK_INCLUDE
#include APP_INCLUDE

MeshNet* mesh;
STACK stack;
APP app;

void user_setup() {
    mesh = new MeshNet();
    app.use(&stack);
    app.use(mesh);
    stack.begin();
}

void user_loop() {
    yield();
}

#endif