#ifdef RUN_MODE_STANDALONE

#include <WiFiNet.h>

#include STACK_INCLUDE
#include APP_INCLUDE

WiFiNet wifiNet;
STACK stack;
APP app;

void user_setup() {
    wifiNet.use(&stack)->connect();
    app.use(&stack);
    stack.begin();
}

void user_loop() {
    yield();
}

#endif