#!/bin/bash

rm -rf dist

# build firmware
docker run -it --rm --name platformio \
    -v "${PWD}":/usr/src/app \
    registry.gitlab.com/wirelos/contraption-pipeline/platformio:v1 \
    pio run $1

mkdir -p dist
cp /home/master/src/sprocket-template/.pioenvs/build/firmware.bin dist