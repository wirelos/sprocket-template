#!/bin/bash

source ../.{{.app}}

# deploy mqtt ota
docker run -it --rm  \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v "${PWD}"/.pioenvs/build:/usr/src/app/bin \
    registry.gitlab.com/wirelos/contraption-pipeline/ota-mqtt:v1 \
    --id $FIRMWARE_ID --topic $TOPIC- \
    --bin dist/firmware.bin \
    --broker $BROKER \
    $1