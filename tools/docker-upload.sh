#!/bin/bash

docker run -it --rm --name platformio \
    -v "${PWD}":/usr/src/app \
    registry.gitlab.com/wirelos/contraption-pipeline/platformio:v1 \
    pio run -t upload